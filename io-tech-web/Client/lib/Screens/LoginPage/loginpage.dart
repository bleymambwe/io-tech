import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';




class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            SizedBox(height: 30),
            Text(
              'Login',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 30),
            CupertinoTextField(
              placeholder: 'Username',
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              decoration: BoxDecoration(
                border: Border.all(color: CupertinoColors.black),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            SizedBox(height: 20),
            CupertinoTextField(
              placeholder: 'Password',
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              decoration: BoxDecoration(
                border: Border.all(color: CupertinoColors.black),
                borderRadius: BorderRadius.circular(8),
              ),
              obscureText: true,
            ),
            SizedBox(height: 20),
            CupertinoButton.filled(
              onPressed: () {
                  Navigator.pushNamed(context, '/dashboard');
              },
              child: Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
