import 'package:flutter/material.dart';


class HistoryCard extends StatelessWidget {
  const HistoryCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 300,
        width: 600,
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.shade50,
              offset: Offset(4, 4),
              spreadRadius: 1,
              blurRadius: 1,
            ),
            BoxShadow(
              color: Colors.grey.shade100,
              offset: Offset(-4, -4),
              spreadRadius: 1,
              blurRadius: 1,
            ),
          ],
        ),
        child: CustomTabBar()
    );
  }
}

class CustomTabBar extends StatefulWidget {
  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {
  int currentIndex = 0;
  final List<String> tabTitles = [
    'Overspeeding',
    'Harsh Acceleration',
    'Driving Hours',
    'Night Driving Ban',
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white70,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade50,
                offset: Offset(4, 4),
                spreadRadius: 1,
                blurRadius: 1,
              ),
              BoxShadow(
                color: Colors.grey.shade100,
                offset: Offset(-4, -4),
                spreadRadius: 1,
                blurRadius: 1,
              ),
            ],
          ),
          height: 50,
          //color: Colors.grey.shade200,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(
              tabTitles.length,
                  (index) => GestureDetector(
                onTap: () {
                  setState(() {
                    currentIndex = index;
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    color: currentIndex == index ? Colors.grey.shade500 : Colors.transparent,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    tabTitles[index],
                    style: TextStyle(
                      fontSize: 15,
                      color: currentIndex == index ? Colors.grey.shade900 : Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 20),
        Container(
          height: 200,
          child: IndexedStack(
            index: currentIndex,
            children: [
              buildTabContent('Content for Overspeeding'),
              buildTabContent('Content for Harsh Acceleration'),
              buildTabContent('Content for Driving Hours'),
              buildTabContent('Content for Night Driving Ban'),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildTabContent(String text) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    );
  }
}