import 'package:flutter/material.dart';
import 'VehicleListServer/vehicle_list_server.dart';

class VehicleListBar extends StatefulWidget {
  final String jsonData;

  VehicleListBar({Key? key, required this.jsonData}) : super(key: key);

  @override
  State<VehicleListBar> createState() => _VehicleListBarState();
}

class _VehicleListBarState extends State<VehicleListBar> {
  late List<VehicleData> vehicleList;

  @override
  void initState() {
    super.initState();
    vehicleList = processVehicleData(widget.jsonData);
  }

  @override
  void didUpdateWidget(VehicleListBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.jsonData != oldWidget.jsonData) {
      vehicleList = processVehicleData(widget.jsonData);
    }
  }

  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: vehicleList.length,
          itemBuilder: (context, index) {
            VehicleData vehicleData = vehicleList[index];
            return ListCard(
              vehicleName: vehicleData.vehicleName,
              overspeedingMsg: vehicleData.overspeedingMsg,
              hashAccelerationMsg: vehicleData.hashAccelerationMsg,
              drivingHoursMsg: vehicleData.drivingHoursMsg,
              nightDrivingBanMsg: vehicleData.nightDrivingBanMsg,
            );
          },
        ),
      ),
    );
  }
}

class ListCard extends StatelessWidget {
  final String vehicleName;
  final String overspeedingMsg;
  final String hashAccelerationMsg;
  final String drivingHoursMsg;
  final String nightDrivingBanMsg;

  ListCard({
    required this.vehicleName,
    required this.overspeedingMsg,
    required this.hashAccelerationMsg,
    required this.drivingHoursMsg,
    required this.nightDrivingBanMsg,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey.shade50,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          MessageBox(text: vehicleName, width: 80),
          MessageBox(text: overspeedingMsg),
          MessageBox(text: hashAccelerationMsg),
          MessageBox(text: drivingHoursMsg),
          MessageBox(text: nightDrivingBanMsg),
        ],
      ),
    );
  }
}

class MessageBox extends StatelessWidget {
  final String text;
  final double width;

  const MessageBox({required this.text, this.width = 300});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 100,
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade50,
            offset: Offset(4, 4),
            spreadRadius: 1,
            blurRadius: 1,
          ),
          BoxShadow(
            color: Colors.grey.shade100,
            offset: Offset(-4, -4),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ),
      child: Center(
        child: Text(
          text,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}


