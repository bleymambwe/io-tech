import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

import '../VehicleListView/vehicle_listview.dart';
//import '../VehicleJsonDatabase/vehicle_json_database.dart';

class VehicleListCard extends StatelessWidget {
  const VehicleListCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      width: 1300,
      decoration: BoxDecoration(
        color: Colors.grey.shade50,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        children: [
          //title
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: 100,
                height: 100,
                padding: EdgeInsets.all(6),
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Text(
                    'Name',
                    style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(6),
                width: 300,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Text(
                    'Overspeeding',
                    style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(6),
                width: 300,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Text(
                    'Hash Acceleration',
                    style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Container(
                width: 300,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Text(
                    'Driving Hours',
                    style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Container(
                width: 300,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Text(
                    'Night Driving Ban',
                    style: TextStyle(
                      color: Colors.grey.shade900,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
            ],
          ),
          //vehicle list bar
          VehicleListBarWidget(),
        ],
      ),
    );
  }
}

class JsonDataProvider {
  final String jsonData;

  JsonDataProvider(this.jsonData);

  String getJsonData() {
    return jsonData;
  }

  Map<String, dynamic> toJson() {
    return jsonDecode(jsonData);
  }
}

String jsonData = '''
{
  "id_0": {
    "vehicle_name": "American Bus",
    "total_violations": 2,
    "attributes": {
      "overspeeding": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "hashacceleration": {
        "violations": null,
        "message": [
          "Hash (Decceleration) Device: ABT 1170 Address: Speed: from 65 Kmh to 42 kmh Time: 2023-05-27 06:52:10.027945",
          "Hash Acceleration History"
        ]
      },
      "driving hours": {
        "violations": null,
        "message": ["driving hours offense recorded", "message2"]
      },
      "night driving ban": {
        "violations": null,
        "message": []
      }
    }
  },
  "id_1": {
    "vehicle_name": "vehicle2",
    "total_violations": 1,
    "attributes": {
      "overspeeding": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "hashacceleration": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "driving hours": {
        "violations": null,
        "message": ["driving hours offense recorded", "message2"]
      },
      "night driving ban": {
        "violations": null,
        "message": []
      }
    }
  }
}
''';

JsonDataProvider jsonDataProvider = JsonDataProvider(jsonData);
class VehicleListBarWidget extends StatefulWidget {
  @override
  _VehicleListBarWidgetState createState() => _VehicleListBarWidgetState();
}

class _VehicleListBarWidgetState extends State<VehicleListBarWidget> {
  late ValueNotifier<String> jsonDataNotifier;
  Timer? timer;
  final List<String> vehicleNames = [
    "ABP 1134",
    "ABT 1170",
    "ACK 974 LAND CRUISER",
  ];

  @override
  void initState() {
    super.initState();
    // Initialize the ValueNotifier with initial data
    jsonDataNotifier = ValueNotifier<String>(jsonDataProvider.getJsonData());

    // Start a timer to periodically update the jsonData
    timer = Timer.periodic(Duration(seconds: 1), (_) {
      updateJsonData();
      // print(jsonDataNotifier);
    });
  }

  @override
  void dispose() {
    jsonDataNotifier.dispose();
    timer?.cancel();
    super.dispose();
  }

  void updateJsonData() async {
    final url = 'http://127.0.0.1:8000/get-json';

    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        final responseData = response.body;

        if (responseData != null && responseData.isNotEmpty) {
          try {
            final updatedJsonData = jsonDecode(responseData);

            if (updatedJsonData is Map<String, dynamic>) {
              // Update the jsonDataNotifier with the fetched data
              jsonDataNotifier.value = responseData;
            } else {
              print('Invalid JSON data');
              jsonDataNotifier.value = 'error';
            }
          } catch (error) {
            print('Failed to parse JSON data: $error');
            jsonDataNotifier.value = 'error';
          }
        } else {
          print('Empty response body');
          jsonDataNotifier.value = 'error';
        }
      } else {
        print('Failed to fetch data from the server. Status Code: ${response.statusCode}');
        jsonDataNotifier.value = 'error';
      }
    } catch (error) {
      print('Failed to fetch data: $error');
      jsonDataNotifier.value = 'error';
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<String>(
      valueListenable: jsonDataNotifier,
      builder: (context, jsonData, _) {
        if (jsonData == 'error') {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(height: 16),
              Text('Fetching data...'),
            ],
          );
        } else {
          return VehicleListBar(jsonData: jsonData);
        }
      },
    );
  }
}
