import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class VehicleData {
  final String vehicleName;
  final String overspeedingMsg;
  final String hashAccelerationMsg;
  final String drivingHoursMsg;
  final String nightDrivingBanMsg;

  VehicleData({
    required this.vehicleName,
    required this.overspeedingMsg,
    required this.hashAccelerationMsg,
    required this.drivingHoursMsg,
    required this.nightDrivingBanMsg,
  });
}

List<VehicleData> processVehicleData(String jsonData) {
  List<VehicleData> vehicleList = [];

  // Parse the JSON data
  Map<String, dynamic> vehicleDatabase = jsonDecode(jsonData);

  vehicleDatabase.forEach((id, data) {
    String vehicleName = data['vehicle_name'];
    Map<String, dynamic> attributes = data['attributes'];

    String overspeedingMsg =
    attributes['overspeeding']['message'] != null && attributes['overspeeding']['message'].isNotEmpty
        ? attributes['overspeeding']['message'][0]
        : 'No offense';
    String hashAccelerationMsg =
    attributes['hash_acceleration']['message'] != null && attributes['hash_acceleration']['message'].isNotEmpty
        ? attributes['hash_acceleration']['message'][0]
        : 'No offense';
    String drivingHoursMsg =
    attributes['driving_hours']['message'] != null && attributes['driving_hours']['message'].isNotEmpty
        ? attributes['driving_hours']['message'][0]
        : 'No offense';
    String nightDrivingBanMsg =
    attributes['night_driving_ban']['message'] != null && attributes['night_driving_ban']['message'].isNotEmpty
        ? attributes['night_driving_ban']['message'][0]
        : 'No offense';

    vehicleList.add(VehicleData(
      vehicleName: vehicleName,
      overspeedingMsg: overspeedingMsg,
      hashAccelerationMsg: hashAccelerationMsg,
      drivingHoursMsg: drivingHoursMsg,
      nightDrivingBanMsg: nightDrivingBanMsg,
    ));
  });

  return vehicleList;
}

