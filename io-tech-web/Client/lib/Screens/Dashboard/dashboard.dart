import 'package:flutter/material.dart';
import 'VehicleJsonDatabase/vehicle_json_database.dart';
import 'VehicleListView/vehicle_list_card.dart';
import 'HistoryCard/history_card.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);
  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,

       children: [
         Row(
           mainAxisSize: MainAxisSize.max,
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: [
             Text('Victor Kangwa',
                style: TextStyle(color: Colors.grey.shade900, fontSize: 20,fontWeight: FontWeight.w800 ),
             ),
             SizedBox(width: 55,),
             Text('14:00',
               style: TextStyle(color: Colors.grey.shade900, fontSize: 20, fontWeight:FontWeight.w800  ),
             ),
           ],
         ),

         SizedBox(height: 20,),
         Row(
           mainAxisSize: MainAxisSize.max,
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: [
             OffenseCard(),
             HistoryCard(),
           ],
         ),
         SizedBox(height: 50,),
         VehicleListCard(),
         //VehicleListCard(jsonData: ValueNotifier<dynamic>( jsonData )),

       ],
      ),
    );
  }
}

class OffenseCard extends StatelessWidget {
  const OffenseCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 300,

      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade50,
            offset: Offset(4, 4),
            spreadRadius: 1,
            blurRadius: 1,
          ),
          BoxShadow(
            color: Colors.grey.shade100,
            offset: Offset(-4, -4),
            spreadRadius: 1,
            blurRadius: 1,
          ),
        ],
      ), //
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OffenseCardInfo(text: 'Total Number of Offenses', value: 5,),
            OffenseCardInfo(text: 'Overspeeding Offenses', value: 5,),
            OffenseCardInfo(text: 'Driving Hours Offenses', value: 5,),
            OffenseCardInfo(text: 'Night Driving Ban Offenses', value: 5,),
          ],
        ),
      ),
    );
  }
}


class OffenseCardInfo extends StatelessWidget {
  final String text;
  final int value;

  const OffenseCardInfo({required this.text, required this.value});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
      //text
        Container(
          height: 65,
          width: 120,
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Center(
            child: Text(
              text,
              style: TextStyle(color: Colors.grey.shade900, fontSize: 15),
            ),
          ),
        ),
        SizedBox(width: 5,),
        //value
        Container(
          height: 65,
          width: 55,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),

          child: Center(
            child: Text(
              value.toString(),
              style: TextStyle(color: Colors.grey.shade600, fontSize: 17),
            ),
          ),
        ),
      ],
    );
  }
}
