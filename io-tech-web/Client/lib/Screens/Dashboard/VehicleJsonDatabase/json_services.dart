import 'dart:convert';
import 'dart:math';

class JsonDataService {
  static String updateJsonData(String jsonData, List<String> vehicleNames) {
    final random = Random();
    final updatedJsonData = {
      "id_0": {
        "vehicle_name": vehicleNames[random.nextInt(vehicleNames.length)],
        "total_violations": 2,
        "attributes": {
          "overspeeding": {
            "violations": null,
            "message": ["No Offense", "message2"]
          },
          "hashacceleration": {
            "violations": null,
            "message": [
              "Hash (Decceleration) Device: ABT 1170 Address: Speed: from 65 Kmh to 42 kmh Time: 2023-05-27 06:52:10.027945",
              "Hash Acceleration History"
            ]
          },
          "driving hours": {
            "violations": null,
            "message": ["driving hours offense recorded", "message2"]
          },
          "night driving ban": {
            "violations": null,
            "message": []
          }
        }
      },
      "id_1": {
        "vehicle_name": vehicleNames[random.nextInt(vehicleNames.length)],
        "total_violations": 1,
        "attributes": {
          "overspeeding": {
            "violations": null,
            "message": ["No Offense", "message2"]
          },
          "hashacceleration": {
            "violations": null,
            "message": ["Hash (Acceleration) Device: ACL 9723 QBS 012 Address: T5 Speed: from -7 Kmh to 17 kmh Time: 2023-05-27 07:01:46.770934", "message2"]
          },
          "driving hours": {
            "violations": null,
            "message": ["driving hours offense recorded", "message2"]
          },
          "night driving ban": {
            "violations": null,
            "message": []
          }
        }
      }
    };

    return jsonEncode(updatedJsonData);
  }
}
