import 'dart:convert';

class JsonDataProvider {
  final String jsonData;

  JsonDataProvider(this.jsonData);

  String getJsonData() {
    return jsonData;
  }

  Map<String, dynamic> toJson() {
    return jsonDecode(jsonData);
  }
}

String jsonData = '''
{
  "id_0": {
    "vehicle_name": "London Bus",
    "total_violations": 2,
    "attributes": {
      "overspeeding": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "hashacceleration": {
        "violations": null,
        "message": [
          "Hash (Decceleration) Device: ABT 1170 Address: Speed: from 65 Kmh to 42 kmh Time: 2023-05-27 06:52:10.027945",
          "Hash Acceleration History"
        ]
      },
      "driving hours": {
        "violations": null,
        "message": ["driving hours offense recorded", "message2"]
      },
      "night driving ban": {
        "violations": null,
        "message": []
      }
    }
  },
  "id_1": {
    "vehicle_name": "vehicle2",
    "total_violations": 1,
    "attributes": {
      "overspeeding": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "hashacceleration": {
        "violations": null,
        "message": ["message1", "message2"]
      },
      "driving hours": {
        "violations": null,
        "message": ["driving hours offense recorded", "message2"]
      },
      "night driving ban": {
        "violations": null,
        "message": []
      }
    }
  }
}
''';

JsonDataProvider jsonDataProvider = JsonDataProvider(jsonData);
String encodedJsonData = jsonEncode(jsonDataProvider.toJson());
