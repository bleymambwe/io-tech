import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class HomeContent extends StatelessWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(

      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              // Container(width:155, color: Colors.grey,),
              TextDis(text: 'View Live Updates'),

              ImageDis(image_link:'https://www.dropbox.com/s/7kpuljajplhvvpu/topview_roads.jpg?dl=1' ),
            ],
          ),
          GradientButton(
            text: 'Login to view fleet',
            onPressed: () {
              Navigator.pushNamed(context, '/login');
            },
          ),


          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              ImageDis(image_link:'https://www.dropbox.com/s/yifzrspy6t6qpqj/maps_gif.gif?dl=1' ),
              TextDis(text: 'View your vehicles speed, course,location')
            ],
          )
        ],
      ),
    );
  }
}



class GradientButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  const GradientButton({
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 200,
        height: 50,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.grey, Colors.grey, Colors.grey],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              //fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}


class ImageDis extends StatelessWidget {
  final String image_link;
  const ImageDis({required this.image_link});

  @override
  Widget build(BuildContext context) {

    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.network(
        image_link,
        width: 400,
        height: 400,
        fit: BoxFit.cover,
      ),
    );
  }
}


class TextDis extends StatelessWidget {
  final String text;

  const TextDis({required this.text});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 200,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 25,
            foreground: Paint()
              ..shader = ui.Gradient.linear(
                const Offset(0, 0),
                const Offset(150, 0),
                <Color>[
                  Colors.red,
                  Colors.greenAccent,
                ],
              ),
          ),
        ),
      ),
    );
  }
}