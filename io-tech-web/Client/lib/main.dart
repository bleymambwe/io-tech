import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Screens/LoginPage/loginpage.dart';
import 'Screens/Home/home_page.dart';
import 'Screens/Dashboard/dashboard.dart';


void main() => runApp( MaterialApp(home: HomePage(),debugShowCheckedModeBanner: false, )  );

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            NavBar(),
            SizedBox(height: 55,),
            Expanded(
           // SizedBox(height: 100,),
              child: Navigator(
                onGenerateRoute: (settings) {
                  return MaterialPageRoute(
                    settings: settings,
                    builder: (BuildContext context) {
                      switch (settings.name) {
                        case '/':
                          return DashBoard(); //HomeContent();
                        case '/login':
                          return LoginPage();
                        case '/dashboard':
                          return DashBoard();
                      // Add more routes if needed
                        default:
                          return HomeContent();
                      }
                    },
                  );
                },
              ),
        ),
          ]
        ),
      ),
    );
  }
}


class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            "Home",
            style: TextStyle(color: Colors.grey.shade500, fontSize: 24),
          ),
      Text(
        "View Fleet",
        style: TextStyle(color: Colors.grey.shade500, fontSize: 24),
      ),

      Text(
        "About Us",
        style: TextStyle(color: Colors.grey.shade500, fontSize: 24),
      ),
        ],
      ),
    );
  }
}




