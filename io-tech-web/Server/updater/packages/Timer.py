import time

def start_timer(func, end_time):
    now = 0
    while now < end_time:
        func()
        time.sleep(1)
        now+=1
