import requests

class Vehicle():
    '''contains infomation about vehicles  '''
    def __init__(self,web = 'https://service.packet-v.com/', token ='$2y$10$6/S6yOBbaWldGjvtk9P7g.T3.xX9Z6Iqe59.a4Ji9J2THWDa78Nv6' ):
        self.web = web
        self.token = token
        self.device_url = self.web+'api/get_devices_latest'
        self.update_response()
        #super().__init__()
        #create tuple { id: name, total_hours of driving, non-continous-hours,rest-hours }

    def update_response(self):
        try:
            self.response = requests.get(url = self.device_url, params = {'lang' :'english', 'user_api_hash' :self.token} )
            self.response = self.response.json()
        except ConnectionError as CE:
            print('Connection Error', CE)

    def check_vehicles(self):
        '''
    As of now, the below list is about vehicles
     0: 'ABP 1134'...
        '''

        length = self.num_of_vehicles()
        self.vehicles = {}
        self.online_vehicles = {}

        for i in range(length):
            response = self.response['items'][i]['online']
            self.vehicles[i] = self.response['items'][i]['name'], response if response == 'online' else 'offline'

    def check_vehicles_(self):
        '''
    As of now, the below list is about vehicles
     0: 'ABP 1134'...
        '''
        length = self.num_of_vehicles()
        self.vehicles = {}
        self.online_vehicles = {}

        for i in range(length):
            response = self.response['items'][i]['online']
            self.vehicles[i] = response,self.response['items'][i]['name']

    def num_of_vehicles(self):
        '''
        returns the number of vehicles/devices available
        '''
        return len(self.response['items'])

    def vehicle_name(self,vehicle_num):
        '''
        Returns name or plate number of vehicle
        '''
        return (self.vehicles[vehicle_num])

    def owner_name(self, vehicle_num):
        '''
        Returns name of driver or vehicle owner
        '''
        return (self.response['items'][vehicle_num]['device_data']['object_owner'])

    def phone_number(self, vehicle_num):
        '''
        Returns phone number of driver of owner
        '''
        return (self.response['items'][vehicle_num]['device_data']['sim_number'])

    def fuel_quantity(self, vehicle_num):
        '''
        Returns current fuel quantity
        '''
        self.update_response()
        return (self.response['items'][vehicle_num]['device_data']['fuel_quantity'])

    def total_distance(self, vehicle_num):
        '''
        Returns total distance covered
        '''
        self.update_response()
        return (self.response['items'][vehicle_num]['total_distance'])

    def check_status(self,vehicle_num):
        '''
        checks wheather or not vehicle is online or offline
        '''
        response = self.response['items'][vehicle_num]['device_data']['online']
        return response if response == 'online' else 'offline'

    def coordinates(self,vehicle_num):
        '''
        Returns latest coordinates
        '''
        self.update_response()
        return (self.response['items'][vehicle_num]['lat']),(self.response['items'][vehicle_num]['lng'])

    def fuel_quantity(self,vehicle_num):
        return self.response['items'][vehicle_num]['device_data']['fuel_quantity']

    def fuel_filling(self,vehicle_num):
        '''
        Returns number of times the vehicle has been filled
        '''
        return self.response['items'][vehicle_num]['device_data']['min_fuel_fillings']

    def fuel_price(self,vehicle_num):
        '''
        Returns number of times the vehicle has been filled
        '''
        return self.response['items'][vehicle_num]['device_data']['fuel_per_km']

    def min_speed(self,vehicle_num):
        '''
        returns minimum speed the vehicle moved
        '''
        return self.response['items'][vehicle_num]['device_data']['min_moving_speed']

    def speed(self,vehicle_num):
        '''
        returns current speed of vehicle
        '''
        return self.response['items'][vehicle_num]['device_data']['speed']

    def stop_duration(self,vehicle_num):
        '''
        returns duration on how long the vehicle stopped
        '''
        return self.response['items'][vehicle_num]['stop_duration']

    def altitude(self,vehicle):
        '''
        returns height of landscape of where the vehicle is
        '''
        return self.response['items'][vehicle_num]['atitude']

    def object_(self):
        return self.response['items']
