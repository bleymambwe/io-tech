import requests
import time
#from datetime import datetime,timedelta
#import pytz
#import getpass, smtplib
#from email.message import EmailMessage
#from fastapi import FastAPI

from packages.RC import RoadClassifier as RC
from packages.Email import Mail
from packages.HelperMethods import HelperMethods
from packages.Timer import start_timer
from packages.Poster import Post
from packages.VehicleFunctions import Vehicle
#Double check Road Clasifier
#Extended alerts to record and alarm
#Multi-threading
#Email alert system limit 300 emails per day, 500 at ones
#Make email multimedia- on a ever-loop, on sending emails
#Analyse complexity and improve performance
#Put Vehicle class somewhere else

class NewFeatures(HelperMethods,Vehicle):
    def __init__(self):
        super().__init__()

        self.start_time = self.convert_to_seconds('06:00:00')
        self.end_time = self.convert_to_seconds('18:00:00')
        self.continous_driving_limit =  self.convert_to_seconds('04:30:00') #('00:00:30')
        self.thirty_min = self.convert_to_seconds('00:30:00') #('00:00:20')
        self.fiftn_min  = self.convert_to_seconds('00:15:00')
        self.daily_driving_hours_limit = self.convert_to_seconds('09:30:00')#('00:1:00')

        self.highway_limit = 80
        self.dust_rd_limit = 60
        self.build_up_limit = 40

        self.vehicle_details ={}
        self.overspeeding_vehicles = {}
        self.acc_data = {}

    def streamData(self,increment = 1 ):
        #refresh server
        self.update_response()
        num_of_vehicles = self.num_of_vehicles()
        #print(num_of_vehicles)

        for i in range(num_of_vehicles):

            response = self.response['items'][i]
            curr_time = self.get_current_time()

            #check if vehicle is moving withing approrite time
            if response['speed'] > 0 and curr_time > self.start_time and curr_time < self.end_time:

                if response['id'] not in self.vehicle_details:
                    #vehicle_details = name, driving hours,number of rests,
                    self.vehicle_details[response['id']] = response['name'],0,0

                else:
                    drvn_time = self.vehicle_details[response['id']][1]
                    #update driving hours
                    drvn_time +=increment
                    no_rest=self.vehicle_details[response['id']][2]

                    latitude,longitude = response['lat'],response['lng']

                    #check for overspeeding
                    self.check_for_overspeeding(name = response['name'], curr_speed = response['speed'],latitude= latitude,longitude= longitude,ID = response['id'] )

                    #check if driver has not rested
                    self.daily_driving_rest(driving_hours = drvn_time,rests = no_rest, name = response['name'],latitude = latitude,longitude= longitude )

                    #check for total daily driving hours limit
                    self.daily_driving_hours(driving_hours = drvn_time,name = response['name'],latitude = latitude,longitude= longitude  )

                    #check for acceleration and deacceleration
                    self.check_for_hash_acc(curr_speed = response['speed'],ID = response['id'],name = response['name'],latitude = latitude,longitude= longitude)

                    self.vehicle_details[response['id']] = response['name'],drvn_time,no_rest

            #check if vehicle is driving in unpermited hours
            elif response['speed'] > 0 and curr_time < self.start_time or response['speed'] > 0 and curr_time > self.end_time:
                self.night_driving_ban(response = response)

            #check if vehicle is resting or packing
            elif response['speed'] == 0 and curr_time > self.start_time and curr_time < self.end_time:
                    #record resting hours
                    if response['id'] not in self.vehicle_details:
                        self.vehicle_details[response['id']] = response['name'],0,0
                    else:
                        no_rest = self.vehicle_details[response['id']][2]
                        no_rest+=1
                        #if no_rest>=1:
                            #print('more rests')
                        drvn_time = self.vehicle_details[response['id']][1]
                        self.vehicle_details[response['id']] = response['name'], drvn_time,no_rest
            #vehicle has packed out
            else:
                pass

    def night_driving_ban(self,response):
        name = response['name']
        model = Post()
        mail = Mail()

        latitude,longitude = response['lat'],response['lng']
        current_time = self.get_time_and_date()
        Road = RC()
        address = Road.address(latitude= latitude,longitude = longitude)

        subject = 'Night Driving Ban Alert'
        message = message = (f'Driving Rest \n Device: {name} \n Address: {address}  \n Time: {current_time} \n Driving above bussiness hours')

        model.post(message = message, ext = model.dh)

        mail.send(subject = subject, body = message)

    def check_for_overspeeding(self,name,curr_speed,latitude,longitude,ID):
          Road = RC()
          road = Road.classify(latitude,longitude)
          address = Road.address(latitude= latitude,longitude = longitude)
          current_time= self.get_time_and_date()
          time_now = self.get_current_time()
          subject = 'Overspeeding Alert'
          model = Post()

          mail = Mail()

          #add overspeeding vehicles in datase
          if ID not in self.overspeeding_vehicles and curr_speed >= self.highway_limit :
              #oversp_ve = {id: name,speed}
              self.overspeeding_vehicles[ID] = name,curr_speed,time_now,1
          elif ID in self.overspeeding_vehicles and curr_speed >= self.highway_limit:
              if self.overspeeding_vehicles[ID][3] >=3:
                  if road == 'Highway':
                      if curr_speed >= self.highway_limit:
                          #Record
                          message = (f' Overspeeding (Highway) \n Device: {name} \n Address: {address} \n Speed: {curr_speed} Km\h \n Time: {current_time}')
                          print(message)

                          model.post(message = message, ext = model.ov)

                          mail.send(subject = subject, body = message)

                  elif road == 'Build-Up Area':
                      if curr_speed >= self.build_up_limit:
                          message = (f' Overspeeding (Build-Up Area) \n Device: {name} \n Address: {address} \n Speed: {curr_speed} Km\h \n Time: {current_time}')
                          print(message)

                          model.post(message = message, ext = model.ov)

                          mail.send(subject = subject, body = message)

                  elif road == 'Dust Road':
                      if curr_speed >= self.dust_rd_limit:

                          message = (f' Overspeeding (Dust Road) \n Device: {name} \n Address: {address} \n Speed: {curr_speed} Km\h \n Time: {current_time}')

                          print(message)
                          model.post(message = message, ext = self.ov)

                          mail.send(subject = subject, body = message)
                  else:
                      pass
              else:
                  if (time_now - self.overspeeding_vehicles[ID][2] ) > self.fiftn_min:
                      count = self.overspeeding_vehicles[ID][3]
                      self.overspeeding_vehicles[ID] = name,curr_speed,count+1
              #count


    def check_for_hash_acc(self,curr_speed,ID,name,latitude,longitude):
        model = Post()
        subject = 'Harsh Acceleration Alert'
        mail = Mail()
        Road = RC()
        address = Road.address(latitude= latitude,longitude = longitude)
        current_time = self.get_time_and_date()
        time_now = self.get_current_time()

        #if acceleartion is not in database open a record
        if ID not in self.acc_data:
            self.acc_data[ID] = curr_speed,time_now
            acc = curr_speed
            prev_speed= acc
        #else check how many if acceleration has been for more than 30 seconds
        else:
            #check if acceleration is less than 30 seconds
            prev_speed = self.acc_data[ID][0]
            acc = curr_speed - prev_speed

            prev_time = self.acc_data[ID][1]

            del_time = time_now- prev_time

            self.acc_data[ID] = acc,time_now
            #check if accelerating for than 15 seconds
            if del_time > self.convert_to_seconds('00:00:15') :

                if acc >= 10 :
                    message = (f' Hash (Acceleration) \n Device: {name} \n Address: {address} \n Speed: from {prev_speed} Km\h to {curr_speed} km\h \n Time: {current_time}')
                    print(message)

                    model.post(message = message, ext = model.ha)

                    mail.send(subject = subject, body = message)
                elif acc <= -14:
                    message = (f' Hash (Decceleration) \n Device: {name} \n Address: {address} \n Speed: from {prev_speed} Km\h to {curr_speed} km\h \n Time: {current_time}')
                    print(message)

                    model.post(message = message, ext = model.ha)

                    mail.send(subject = subject, body = message)
                else:
                    pass

    def daily_driving_hours(self,driving_hours,name,latitude,longitude):
        subject = 'Daily Driving Hours Alert'
        model = Post()
        mail = Mail()
        Road = RC()
        address = Road.address(latitude= latitude,longitude = longitude)
        current_time = self.get_time_and_date()

        if driving_hours >= self.daily_driving_hours_limit:
            message = (f'Driving Hours Limit \n Device: {name} \n Address: {address}  \n Time: {current_time} \n You have reached the hours limit')
            print(message)
            model.post(message = message,ext=  model.dh)

            mail.send(subject = subject, body = message)

        elif driving_hours >= self.daily_driving_hours_limit+self.thirty_min:
            message = (f'Driving Hours Limit \n Device: {name} \n Address: {address}  \n Time: {current_time} \n You are 30 minutes above driving hours limit')
            print(message)
            model.post(message = message,ext=  model.dh)

            mail.send(subject = subject, body = message)

        elif driving_hours >= (self.daily_driving_hours_limit + self.thirty_min*2):
            message = (f'Driving Hours Limit \n Device: {name} \n Address: {address}  \n Time: {current_time} \n You are 1 hour above driving hours limit')
            print(message)
            model.post(message = message, ext = model.dh)

            mail.send(subject = subject, body = message)

    def daily_driving_rest(self,driving_hours,rests,name,latitude,longitude):
        subject = 'Daily Driving Rest Alert'
        model = Post()
        mail = Mail()
        Road = RC()
        addres = Road.address(latitude= latitude,longitude = longitude)
        current_time = self.get_time_and_date()

        if driving_hours >= self.continous_driving_limit and rests == 0:
            #message = (f' Recording...{name} Driving Limit before rest Reached, Please take 30 minute break ')
            message = (f'Driving Rest \n Device: {name} \n Address: {address}  \n Time: {current_time} \n Driving Limit before rest Reached, Please take 30 minute break')

            model.post(message = message,ext=  model.dh)

            mail.send(subject = subject, body = message )

        elif driving_hours>= (self.continous_driving_limit+self.thirty_min) and rests == 0:
            message = (f'Driving Rest \n Device: {name} \n Address: {address}  \n Time: {current_time} \n You are driving 30 minutes past the limit, Please take 30 minute break')
            print(message)
            model.post(message = message,ext=  model.dh)

            mail.send(subject = subject, body = message)

        elif driving_hours>= (self.continous_driving_limit+self.thirty_min*2) and rests == 0:
            message = (f'Driving Rest \n Device: {name} \n Address: {address}  \n Time: {current_time} \n You are driving 1 hour past the limit, Please take 30 minute break')
            print(message)
            model.post(message = message,ext=  model.dh)

            mail.send(subject = subject, body = message)
        return


#model = Vehicle()
model = NewFeatures()
end_time =model.convert_to_seconds('24:00:00')*300

start_timer(func = model.streamData, end_time = end_time)
mail = Mail()
mail.send(subject = 'ALERT VIRTUAL MACHINE HAS STOPPED SENDING ALERTS',body = 'Please restart the machine as it has stopped running for the hours it was programmed, Contact programmer for more details.')
